"""
Aplicación web para acortar URLs
Los usuarios de la aplicación podrán especificar URLs, y la aplicación generará para ellos un
recurso aleatorio que, a partir de ese momento, redireccionará la URL correspondiente
"""
from urllib.parse import parse_qs
import webapp
import shelve # Módulo para almacenar datos en memoria
import random # Generador de números aleatorios

# Definición de formularios HTML para responder a las peticiones del navegador.

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Introduce la URL que quieres acortar: </label>
        <input type="text" name="url" required>
      </div>
      <div>
        <input type="submit" value="Enviar">
      </div>
    </form>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en"> 
  <body>
    <div>
      <p>Estas son las URLs ya acortadas: </p>
      <ul>
        {urls}
      </ul>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Recurso no encontrado: {resource}.</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Metodo no permitido: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""

class RandomShort(webapp.webApp): #Inicialización de la clase

    contents = {}
    def parse(self, request):
        """Analiza la solicitud HTTP para extraer el método y el recurso.
        También recoge el cuerpo del mensaje si está presente."""

        method = request.split(' ')[0]
        resource = request.split(' ')[1]
        if method == "POST":
            body = request.split('\r\n\r\n')[1]
        else:
            body = None
        return method, resource, body

    def process(self, data):
        """Procesa la solicitud HTTP y determina qué acción tomar
        en función del método (GET, PUT, POST)"""

        method, resource, body = data

        if method == "GET":
            httpCode, htmlPage = self.get(resource)
        elif method == "POST":
            httpCode, htmlPage = self.post(body)
        else:
            httpCode = "405 Method not allowed"
            htmlPage = PAGE_NOT_ALLOWED.format(method=method)

        return httpCode, htmlPage

    def get(self, resource):
        if resource == "/":
            urls = ", ".join(self.contents.keys()) #Obtener las URLs almacenadas como string separadas por comas
            httpCode = "200 OK"
            htmlPage = PAGE.format(urls=urls, form=FORM)
        elif resource in self.contents: # Si el recurso existe en el diccionario
            new_url = self.contents[resource] #Obtener la URL original asociada al recurso
            httpCode = "302 Found\r\n" + f"Location: {new_url}\r\n"
            htmlPage = "" # No page needed for redirection
        else:
            httpCode = "404 Resource Not Found"
            htmlPage = PAGE_NOT_FOUND.format(resource=resource, form=FORM)
        return httpCode, htmlPage

    def shortener(self, original_url): #Funcion para generar y almacenar URLs acortadas
        if not original_url.startswith("http://") and not original_url.startswith("https://"):
            original_url = "http://" + original_url
        rand = random.randint(1, 1000)
        short_url = f"/{rand}"
        self.contents[short_url] = original_url
        return short_url, original_url

    def generateHTML(self): #Genera el HTML de la página con las URLs acortadas
        urls_html = ""
        for short_url, original_url in self.contents.items():
            urls_html += f"</li>{original_url}</li>\n"
        htmlPage = PAGE.format(urls=urls_html, form=FORM)
        return htmlPage

    def post(self, body):
        body_parts = parse_qs(body)
        if "url" in body_parts:
            original_url = body_parts["url"][0]
            if original_url in self.contents.values():
                short_url = list(self.contents.keys())[list(self.contents.values()).index(original_url)]
                httpCode = "200 OK"
                htmlPage = self.generateHTML()
            else:
                short_url, original_url = self.shortener(original_url)
                httpCode = "200 OK"
                htmlPage = self.generateHTML()
        else:
            httpCode = "422 Unprocessable Entity"
            htmlPage = PAGE_UNPROCESABLE.format(body=body)
        return httpCode, htmlPage

if __name__ == "__main__":
    webApp =  RandomShort("localhost", 1234)

#Class webApp
#Aquí se gestiona el funcionamieto del servidor
import socket

class webApp: #Clase que contiene 3 métodos (funciones relacionados con la clase)

    def parse (self, request):
        #Analizar la solicitud recibida y extrae la información relevante

        return None

    def process (self, parsedRequest):
        #Procesar los elementos relevantes y devuelve el código HTTP con la respuesta

        return ("200 OK", "<html><body><h1>It works!</h1></body></html>")

    def __init__ (self, hostname, port):

        #Inicializa la aplicación web
        mySocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))
        mySocket.listen(5)

        #Gestión de la conexión
        while True:
            print('Servidor esperando peticiones')
            (recvSocket, address) = mySocket.accept()
            print('Se ha recibido esta petición HTTP (se procede a parsear y procesar):')
            request = recvSocket.recv(2048)
            print(request)

            parsedRequest = self.parse(request.decode('utf8'))
            (returnCode, htmlAnswer) = self.process(parsedRequest)

            print('Respondiendo...')
            response = "HTTP/1.1 " + returnCode + " \r\n\r\n" + htmlAnswer + "\r\n"
            recvSocket.send(response.encode('utf8'))
            recvSocket.close()

if __name__ == "__main__":
    testWebApp = webApp ("localhost", 1234) #Se instancia un objeto/clase